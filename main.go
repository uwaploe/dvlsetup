// Dvlsetup initializes an RDI DVL using a file of commands.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	dvl "bitbucket.org/uwaploe/go-dvl"
)

const Usage = `Usage: dvlsetup [options] file host:port

Read a file of RDI DVL commands and send them to a DVL via its TCP
command interface at host:port
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	timeout   time.Duration = time.Second * 5
	traceFile string
	keepAlive bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&timeout, "timeout", timeout,
		"Timeout for DVL command response")
	flag.StringVar(&traceFile, "trace", traceFile,
		"Save the DVL responses to a file")
	flag.BoolVar(&keepAlive, "keep-alive", keepAlive,
		"Keep the TCP connection open after sending commands")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	file, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Cannot open command file: %v", err)
	}
	defer file.Close()

	sensor, err := dvl.Dial(args[1], dvl.ReadTimeout(timeout))
	if err != nil {
		log.Fatal(err)
	}

	if err := sensor.Wakeup(); err != nil {
		log.Fatalf("Cannot wakeup sensor: %v", err)
	}

	sensor.SyncClock(true)

	trace, err := sensor.Upload(file)
	if err != nil {
		log.Fatal(err)
	}

	if traceFile != "" {
		f, err := os.Create(traceFile)
		if err != nil {
			log.Fatalf("Cannot open trace file: %v", err)
		}
		defer f.Close()
		enc := json.NewEncoder(f)
		enc.Encode(trace)
	}
	file.Close()

	if keepAlive {
		// Initialize signal handler
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		defer signal.Stop(sigs)

		log.Printf("DVL keep-alive service starting %s", Version)
		<-sigs
	}
}
